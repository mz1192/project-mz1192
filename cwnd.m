mout_file_not_empty = 0; %if the mout.txt file is not empty, change to 1.
    subplot(2,1,1)

if(mout_file_not_empty)
    datak = dlmread('~/kout.txt');%path of the kout.txt file
    datam = dlmread('~/mout.txt');%path of the mout.txt file
    t1 = [datak(:,1);datam(:,1)];
    [t1, order] = unique(t1);

    c = [datak(:,2);datam(:,2)*1e3];
    c = c(order);
    plot(t1,c)

else
    datak = dlmread('~/kout.txt');
    t1 = datak(:,1);
    c = datak(:,2);
    plot(t1,c)
end
xlabel('Time (seconds)');
ylabel('CWND (KBytes)');
legend('highspeep TCP (buffer size = BDP)')




subplot(2,1,2)
q = load('queue.txt'); %path of the queue.txt file
q = q/1e3;
q(q<1.1) = q(q<1.1)*1e3;
%if you see the queue plot is not reasonbale, it is becasue of the unit of queue maybe bytes, kbytes and mbytes, try the one of following commands.
%do not use the following 2 commands at the same time, try 1 at a time.
% q(q<1.5) = q(q<1.5)*1e3;
% q(q<15) = q(q<15)*1e3;



t = 1/length(q):120/length(q):120;
plot(t,q,'b', 'linewidth', 2);
xlabel('Time (seconds)');
ylabel('Queue Size (KBytes)');
legend('highspeep TCP (buffer size = BDP)')