# Introduction #

In this report, I am going to reproduce the following experiments: 1) throughput as function of buffer size; 2) window behavior of two HighSpeed TCPs and drop tail queue size in under-buffered and over-buffered case as described in [1]. Moreover, I would like to also conduct the experiments with CUBIC congestion control algorithm (in addition to HSTCP).

The reproduced results show that even though the HighSpeed TCP throughput curve follows the same trend as presented in the original paper, the reproduced result achieves higher throughput compares to simulation. Moreover, CUBIC outperforms HighSpeed TCP in this specific case. For the RTT fairness experiment, I get a totally contradictory result, which is small buffer size help congestion window converge faster. One reason for this disagreement might be the original paper mistakenly used regular congestion control algorithm. The last finding is CUBIC shows a better RTT fairness than HighSpeed TCP in both under-buffered and over-buffered case.

# Background #

In order to understand how congestion control mechanism affects the utilization in different buffer size, it is important to know how congestion control reacts after packet loss event. The regular TCP use a congestion window to limit the number of outstanding packets (packets can be send without receiving acknowledgment). When packet loss event happens, the congestion window is reduced to half to reduce sending rate. After that, fast retransmission is triggered and congestion avoidance phase in started. The regular TCP use a additive linear increment, which is slow. In order to achieve high sending rate faster, in congestion avoidance phase, HighSpeed TCP implemented a more rapid linear inflation, Cubic used aggressive cubic function growth. Different congestion window behavior results different link utilization and RTT fairness, which are further discussed in the following.

To understand the buffer effects on utilization, assume a packet arrives at a bottleneck link, a link with arrival rate of all flows larger than the link capacity,  the packet may get buffered or dropped since it can not be transmitted immediately. Moreover, if the sender apply congestion control mechanism, such as TCP, buffer size might affect the bottleneck link utilization. To be more specific, small buffer cause more packet loss, therefore, the sender keep halving congestion window and reduce throughput, but big buffer might introduce large delay. As a result, choosing the optimal buffer size becomes a crucial research. A closed form solution of utilization given buffer size was proposed in [1]. In this report, I would like to validate the simulation results with GENI testbed experiment. Moreover, instead of HighsSpeed TCP, it might be useful to compare the results applying CUBIC congestion control.

For the RTT fairness, bandwidth usage between competing TCP flows is a function of RTT, with short flows receiving more bandwidth than longer ones. This in intuitive since the flow with shorter RTT increases congestion window more rapidly, and larger congestion window allows sending more packets per RTT. This RTT unfairness may be alleviated by forcing two congestion window converge to a similar size. Therefore, for the competing flows with different RTTs, fast congestion window convergence means a more fair share of the bandwidth.

#  Results #

The reproduced results show that even though the HighSpeed TCP throughput curve follows the same trend as presented in the original paper, the reproduced result achieves higher throughput compares to simulation. Moreover, CUBIC outperforms HighSpeed TCP in this specific case. For the RTT fairness experiment, I get a totally contradictory result, which is small buffer size help congestion window converge faster. One reason for this disagreement might be the original paper mistakenly used regular congestion control algorithm. The last finding is CUBIC shows a better RTT fairness than HighSpeed TCP in both under-buffered and over-buffered case.

## Figure 1 and 2 ##

From the following figures, both of the original figures show a gap between the model and simulation result, the paper explains the discrepancy between the two curves in mainly arises from M/M/1/K assumption and simplifying approximations in deriving the closed-form throughput expression. However, my reproduced testbed result achieves a higher throughput than simulation applying HighSpeed congestion control, which matches the analytical model more. The CUBIC outperform HighSpeed TCP in this specific case. One reason I achieves a higher utilization is because the RTT of all the 10 flows are fixed not drawn from random variable. Moreover, I highly doubt the original actually used HighSpeed TCP, it might use regular TCP and obtained a poor performance. I will further prove my assumption in the next section.

-------------figure 1
![fig1.png](https://bitbucket.org/repo/ABMgza/images/4000897320-fig1.png)
-------------figure 2
![fig2.png](https://bitbucket.org/repo/ABMgza/images/329109545-fig2.png)

## Figure 3 and 4 ##

As the following figure showed, the original paper shows HighSpeed TCP congestion window converges faster with large buffer size. However, I highly doubt this observation since the congestion window does not behaves like HighSpeed TCP, e.g., after loss event, HighSpeed TCP should reduce congestion window less than half, but the window plot from the original paper always half the congestion window after loss event. I am suspicious that they did not use HighSpeed TCP in the simulation, further explains why their simulation results did not match the model in the previous section.

In GENI testbed experiment, I get a contradictory result, which shows small buffer helps HighSpeed TCP congestion window converge. I believe the original paper used Regular TCP. Moreover, as you can see from the queue size, when the max queue size is small, both simulation and testbed results empty the queue after buffer overflow. However, for the large buffer case, even though the original simulation results also empty queue, the testbed results did not and remain half full. This is due to the congestion window drops more in the simulation and results smaller sending rate. The last thing is CUBIC shows a faster window convergence in both small and large buffer case, which means CUBIC has a better RTT fairness than HighSpeed TCP in this specific experiment.

-------------figure 3
![fig3.png](https://bitbucket.org/repo/ABMgza/images/1089984594-fig3.png)
-------------figure 4
![fig4.png](https://bitbucket.org/repo/ABMgza/images/1994456283-fig4.png)


# Run My Experiment #

## Figure 1 ##
Build the topology in GENI use the provided file **project_mz1192_request_rspec.xml**. After all the nodes are ready, at your local terminal, for MAC machine, use the following to install csshx.

```
#!terminal
brew install csshx
```
logging into all the nodes including 10 servers, 2 routers and 10 devices use the following commands.

```
#!terminal
csshx ADDRESS1:PORT_NUMBER1 ADDRESS2:PORT_NUMBER2
```
If you failed to logging the nodes, try to use ssh to logging one of the nodes with private key and then you would be able to use csshx.
```
#!terminal
ssh -i PATH_TO_PRIVATE_KEY -p PORT_NUMBER ADDRESS
```

At all nodes, run the following commands to install perf traffic generator,


```
#!terminal

sudo apt-get update # refresh local information about software repositories
sudo apt-get install iperf
```

After that, you can try to test the bandwidth of the links. Assume you want to test the link between two nodes n1 and n2. At n1, run the following command to start a iperf server.


```
#!terminal

iperf -s

```
Then at n2, start a tcp flow use the following,

```
#!terminal

iperf -c IP_OF_N1

```
When the process terminates, it will return the bandwidth of this n1-n2 link. For my case, all the links are 100Mbps.



Now we need to set the delay of all the links. At all the server nodes, run


```
#!terminal

ifconfig
```

and find out which ethernet interface is connected to the router, in my case is eth1. After find the connecting interface, run the following command to add delay to the all the interface


```
#!terminal

sudo tc qdisc add dev eth1 root netem delay DELAY_MS

```

The value of DELAY_MS for server node s1 to s10 is 32.5ms, 33ms, 33.5ms, ..., 37ms, respectively. At all the device nodes, do the same thing and set the delay for d1 to d10 to be 32.5ms, 33ms, 33.5ms, ..., 37ms. Finally at r2, add 50ms delay to the interface connecting to the r1.

After all this setting, ping r2 at r1, you should get 50ms delay. Ping d1 at s1, you should get around 115ms, ... , ping d10 at d10, you should get 124ms delay.


At r1, find the ethernet interface connecting to r2, in my case eth1, and run the following command to set buffer size.


```
#!terminal
sudo tc qdisc del dev eth1 root
sudo tc qdisc replace dev eth1 root tbf rate 100mbit limit BUFFER_SIZE burst 32kB

```

where BUFFER_SIZE equals BDP = 1.25mb.

At all device nodes, d1 to d10, run the following command to start iperf server.


```
#!terminal

iperf -s

```

At all server nodes, s1 to s10, change the congestion algorithm to high speed tcp use the following command.

```
#!terminal

echo highspeed | sudo tee /proc/sys/net/ipv4/tcp_congestion_control

```

run the following command to establish tcp connection to the corresponding device node, which means s1 should connect to d1, and so on. Use the following command to send packets,

```
#!terminal

iperf -c IP_OF_DESTINATION -t 120 -l 1000

```

where 120 means run for 2 minutes, 1000 limit the packet size to be smaller than 1000 Bytes. You also should run the command at the same time use csshx.
After all the process terminates, you will see a bandwidth value at all 10 device nodes and a bandwidth value at all 10 sever nodes. Sum the throughput of all 10 bandwidth at device nodes, denoted as Tput_d. The utilization can be computed by dividing Tput_d by the physical bandwidth of the bottle neck link, which is 100Mbps in may case.

Repeat the experiments multiple time with different buffer size, such as 1.25mb, 0.625mb, 0.25mb, 0.125mb, 0.0625mb, 0.0375mb, 0.0125mb, 0.00625mb.

After repeat the experiment with different buffer size, change the congestion control algorithm to Cubic use the following,

```
#!terminal

echo cubic | sudo tee /proc/sys/net/ipv4/tcp_congestion_control

```
and repeat the process for cubic again.

Then you should be able to have utilization of different buffer size for cubic and high speed TCP. Put the traces into the matlab file **plotUti.m** to plot Figure 1.

## Figure 2 ##

The procedure for figure 2 is similar as figure 1, but this time the delay for the links are different. You may need this command to remove the configurations for each link.


```
#!terminal

sudo tc qdisc del dev ETHERNET_INTERFACE root

```
Change the delay of the links as the following, the delay from r1 to r2 is 10ms, the delay from s1, s2, ... , s10 to r1 is 15ms, 20ms, ... , 60ms, and the delay from d1, d2, ... , d10 to r2 is also 15ms, 20ms, ... , 60ms. The buffer size should within the range of 250KB, 125KB, 50KB, 25KB, 12.5KB 7.5KB and 5KB.

You should be able to get utilization with the same steps above.

## Figure 3&4 ##

For this experiment, we need a different topology, you can directly upload the topology from file **topo2.xml**. After all the nodes are ready, logging into all the nodes use csshx again. At 2 servers and 2 devices Run the following commands to install iperf3.

```
#!terminal
sudo export HTTPS_PROXY="http://10.0.0.200:3128"  
sudo git clone https://github.com/esnet/iperf.git  
cd ~/iperf  
sudo ./bootstrap.sh
sudo ./configure
sudo make  
sudo make install  
sudo ldconfig  
cd  
```

Configure the delay between g1 to g2 as 10ms, delay of s1-g1 link and g2-d1 link is 45ms, delay of s2-g1 link and g2-d2 is 20ms. Make sure to switch to Cubic congestion control algorithm before start. Then copy the **queuemonitor.sh** file to g1 node, and make it excitable (chmod a+x queuemonitor.sh). Then set the buffer size of r1 to be 12.5MB by the following,


```
#!terminal

sudo tc qdisc replace dev eth1 root tbf rate 100mbit limit 12.5Mb burst 32kB

```
where eth1 is the interface connecting g2.

Start iperf3 server at 2 devices use the following command,

```
#!terminal
iperf3 -s
```

Then start sending tcp packets from 2 servers to 2 devices use the following commands,

```
#!terminal
iperf3 -c DESTINATION_IP_ADDRESS -i 0.1 -t 120 | tee NODE_NUMBER.txt  
```
where NODE_NUMBER.txt maybe node1.txt at server1, node2.txt at server2. Immediately execute this common at g1 after tcp flow begins,
```
#!terminal

./queuemonitor.sh eth1 120 0.1 | tee router.txt
```

when the execution terminates, run the following command at g1


```
#!terminal

cat router.txt | sed 's/\p / /g' | awk  '{print $31}' > queue.txt

```
When this is done, copy all the files to a local machine, you should have 3 files. 2 iperf3 log file and 1 queue.txt file.

Go to the path of the folder and run the following commands, where you can get the congestion window for sever 1 and by changing NODE_NUMBER to node1, similar you can get the congestion window for server 2.

```
#!terminal
grep Bytes NODE_NUMBER.txt | awk '{print $3, $10, $11}' | grep KBytes | sed 's/-/ /' | awk '{print $1, $3}' > kout.txt
grep Bytes NODE_NUMBER.txt | awk '{print $3, $10, $11}' | grep MBytes | sed 's/-/ /' | awk '{print $1, $3}' > mout.txt
```

Use the matlab code **cwnd.m** to plot congestion window, you need to set the parameter mout_not_empty to 1 if mout.txt file is not empty, otherwise it is 0. Finally you could plot the congestion window of the two servers, along with the queue length. Then here you should have the plots of congestion window and queue length of 12.5 buffer case with Cubic congestion control.

Repeat the experiment, but change buffer size to 1.25mb.

Finally switch the congestion control to high speed tcp and repeat the above process. Then you should have 4 plots, Cubic 12.5 mb buffer, Cubic 1.25mb buffer, Highpeed 12.5mb buffer and Highspeed 1.25mb buffer.


# Notes #

System information: Linux version 3.13.0-33-generic (buildd@tipua) (gcc version 4.8.2 (Ubuntu 4.8.2-19ubuntu1) )

GENI aggregate: GPO InstaGENI

Software: Matlab R_2014b

The original simulations use some high-capacity link (greater than 1Gbps), since the maximum bandwidth of the links are 100Mbps, I scale down all the link bandwidth to 100Mbps and adjust the corresponding parameter. The rest is consistent to the paper.

# Reference #

[1] D. Barman, G. Smaragdakis and I. Matta, The effect of router buffer size on HighSpeed TCP performance, Global Telecommunications Conference, 2004. GLOBECOM 04. IEEE, 2004, pp. 1617-1621 Vol.3.