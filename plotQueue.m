%plot the queue size
figure
x = load('hhnew.txt'); %path of txt file - high speed tcp with large buffer
t = 0.1:0.1:length(x)*0.1;
plot(t,x,'b', 'linewidth', 2);
xlabel('Time(seconds)');
ylabel('Queue Size(Packet)');
legend('highspeep TCP (buffer size = BDP)')
figure
x = load('hlnew.txt');%path of txt file - high speed tcp with small buffer
t = 0.1:0.1:length(x)*0.1;
plot(t,x,'b', 'linewidth', 2);
xlabel('Time(seconds)');
ylabel('Queue Size(Packet)');
legend('highspeep TCP (buffer size = 10%BDP)')
figure
x = load('chnew.txt');%path of txt file - cubic with large buffer
t = 0.1:0.1:length(x)*0.1;
plot(t,x,'b', 'linewidth', 2);
xlabel('Time(seconds)');
ylabel('Queue Size(Packet)');
legend('Cubic (buffer size = BDP)')
figure
x = load('clnew.txt');%path of txt file - cubic with small buffer
t = 0.1:0.1:length(x)*0.1;
plot(t,x,'b', 'linewidth', 2);
xlabel('Time(seconds)');
ylabel('Queue Size(Packet)');
legend('Cubic (buffer size = BDP)')