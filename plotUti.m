%plot the ultilization
close all
defaultBW = 100; %bandwidth in mbps
%For figure 1, which the r1-r2 link is 100mbps 50 ms delay.
%Cubic
buffer = [0.00625, 0.0125, 0.0375, 0.0625, 0.125, 0.25, 0.625, 1.25]; %buffer size in MB
Cubicdata = [91.53, 91.53, 92.47, 93.24, 94.56, 95.1, 95.48, 95.41];

%high spped tcp
Highspeeddata =   [86.48, 87.95, 89.25, 90.08, 91.73, 94.69, 95.38, 95.45];

x = buffer/buffer(end);
Cubicy = Cubicdata/defaultBW;
Highspeedy = Highspeeddata/defaultBW;
plot(x, Cubicy, '-sb', x, Highspeedy, '-^r', 'linewidth', 2);
xlabel('Fraction of buffer size 1250KB');
ylabel('Ultilization');
legend('Cubic', 'High Speed');
grid on
%For figure 2, which the r1-r2 link is 100mbps 10 ms delay.
%Cubic
figure
buffer = [5 7.5 12.5 25 50 125 250]; %buffer size in KB
Cubicdata =   [90.95 93.61 93.42 93.57 94.74 95.45 95.28];

%high spped tcp
Highspeeddata =   [87.23 90.96 90.95 92.04 93.31 94.87 95.4];

x = buffer/buffer(end);
Cubicy = Cubicdata/defaultBW;
Highspeedy = Highspeeddata/defaultBW;
plot(x, Cubicy, '-sb', x, Highspeedy, '-^r', 'linewidth', 2);
xlabel('Fraction of buffer size 250KB');
ylabel('Ultilization');
legend('Cubic', 'High Speed');
grid on
